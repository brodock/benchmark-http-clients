$:.unshift File.join(__dir__, 'lib')
require 'experiments'

EXPERIMENTS = [:typhoeus, :httprb, :async_http, :net_http, :curb, :rest_client, :httparty, :httpclient, :excon]

desc 'Benchmark experiments: download operation'
task :download do
  EXPERIMENTS.each do |experiment_task|
    Rake::Task["download:#{experiment_task}"].invoke
  end
end

desc 'Benchmark experiments: upload operation'
task :upload do
  EXPERIMENTS.each do |experiment_task|
    Rake::Task["upload:#{experiment_task}"].invoke
  end
end

namespace :download do
  task :async_http do
    require 'experiments/async_http'

    Experiments::AsyncHTTP.new.download
  end

  task :curb do
    require 'experiments/curb'

    Experiments::Curb.new.download
  end

  task :excon do
    require 'experiments/excon'

    Experiments::Excon.new.download
  end

  task :httpclient do
    require 'experiments/httpclient'

    Experiments::HTTPClient.new.download
  end

  task :httparty do
    require 'experiments/httparty'

    Experiments::HTTParty.new.download
  end

  task :httprb do
    require 'experiments/httprb'

    Experiments::HTTPRB.new.download
  end

  task :net_http do
    require 'experiments/net_http'

    Experiments::NetHTTP.new.download
  end

  task :rest_client do
    require 'experiments/rest_client'

    Experiments::RestClient.new.download
  end

  task :typhoeus do
    require 'experiments/typhoeus'

    Experiments::Typhoeus.new.download
  end
end

namespace :upload do
  task :async_http do
    require 'experiments/async_http'

    Experiments::AsyncHTTP.new.upload
  end

  task :curb do
    require 'experiments/curb'

    Experiments::Curb.new.upload
  end

  task :excon do
    require 'experiments/excon'

    Experiments::Excon.new.upload
  end

  task :httpclient do
    require 'experiments/httpclient'

    Experiments::HTTPClient.new.upload
  end

  task :httparty do
    require 'experiments/httparty'

    Experiments::HTTParty.new.upload
  end

  task :httprb do
    require 'experiments/httprb'

    Experiments::HTTPRB.new.upload
  end

  task :net_http do
    require 'experiments/net_http'

    Experiments::NetHTTP.new.upload
  end

  task :rest_client do
    require 'experiments/rest_client'

    Experiments::RestClient.new.upload
  end

  task :typhoeus do
    require 'experiments/typhoeus'

    Experiments::Typhoeus.new.upload
  end
end

desc 'Start server'
task :server do
  system("rackup app.ru -s puma")
end
