class Experiments::Typhoeus
  include Experiments

  def download
    profile("Typhoeus", executions: EXECUTIONS, benchmark_path: BENCHMARK_DOWNLOAD_PATH) do
      ::Typhoeus::Request.new(DOWNLOAD_URL).tap do |req|
        req.on_body do |chunk|
          chunk.clear
        end

        req.run
      end
    end
  end

  def upload
    profile("Typhoeus", executions: EXECUTIONS, benchmark_path: BENCHMARK_UPLOAD_PATH) do
      tempfile.rewind
      
      ::Typhoeus.post(UPLOAD_URL, body: { file: tempfile })
    end
  end
end