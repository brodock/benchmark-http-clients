class Experiments::NetHTTP
  include Experiments

  def download
    profile("Net::HTTP", executions: EXECUTIONS, benchmark_path: BENCHMARK_DOWNLOAD_PATH) do
      uri = URI.parse(DOWNLOAD_URL)
    
      Net::HTTP.start(uri.host, uri.port) do |http|
        http.request_get(uri.request_uri) do |response|
          response.read_body { |chunk| chunk.clear }
        end
      end
    end
  end

  def upload
    profile("Net::HTTP", executions: EXECUTIONS, benchmark_path: BENCHMARK_UPLOAD_PATH) do
      uri = URI.parse(UPLOAD_URL)
      Net::HTTP.start(uri.host, uri.port) do |http|
        post = Net::HTTP::Post.new(uri.request_uri)
        post.set_form({ "file" => File.open(tempfile_path) }, "multipart/form-data")
    
        http.request(post)
      end
    end
  end
end