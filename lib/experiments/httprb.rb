class Experiments::HTTPRB
  include Experiments

  def download
    profile("http.rb", executions: EXECUTIONS, benchmark_path: BENCHMARK_DOWNLOAD_PATH) do
      response = HTTP.get(DOWNLOAD_URL)
      response.body.each { |chunk| chunk.clear }
    end
  end

  def upload
    profile("http.rb", executions: EXECUTIONS, benchmark_path: BENCHMARK_UPLOAD_PATH) do
      HTTP.post(UPLOAD_URL, form: { file: HTTP::FormData::File.new(tempfile_path) })
    end
  end
end