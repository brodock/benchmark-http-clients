class Experiments::RestClient
  include Experiments

  def download
    profile("RestClient", executions: EXECUTIONS, benchmark_path: BENCHMARK_DOWNLOAD_PATH) do
      ::RestClient::Request.execute(
        method: :get,
        url: DOWNLOAD_URL,
        block_response: -> (response) { response.read_body { |chunk| chunk.clear } })
    end
  end

  def upload
    profile("RestClient", executions: EXECUTIONS, benchmark_path: BENCHMARK_UPLOAD_PATH) do
      ::RestClient.post(UPLOAD_URL, file: File.open(tempfile_path))
    end
  end
end