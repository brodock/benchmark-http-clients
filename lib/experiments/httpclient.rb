class Experiments::HTTPClient
  include Experiments

  def download
    profile("HTTPClient", executions: EXECUTIONS, benchmark_path: BENCHMARK_UPLOAD_PATH) do
      ::HTTPClient.new.get_content(DOWNLOAD_URL) do |chunk|
        chunk.clear
      end
    end
  end

  def upload
    profile("HTTPClient", executions: EXECUTIONS, benchmark_path: BENCHMARK_DOWNLOAD_PATH) do
      tempfile.rewind
      
      ::HTTPClient.new.post(UPLOAD_URL, body: { 'upload' => tempfile })
    end
  end
end