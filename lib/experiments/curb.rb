class Experiments::Curb
  include Experiments

  def download
    profile("Curb", executions: EXECUTIONS, benchmark_path: BENCHMARK_DOWNLOAD_PATH) do
      delete_file = Tempfile.new
    
      Curl::Easy.download(DOWNLOAD_URL, delete_file)
    
      delete_file.close
    end
  end

  def upload
    profile("Curb", executions: EXECUTIONS, benchmark_path: BENCHMARK_UPLOAD_PATH) do
      Curl::Easy.new(UPLOAD_URL).tap do |curl|
        curl.multipart_form_post = true
        curl.http_post(Curl::PostField.file('upload', tempfile_path))
      end
    end
  end
end