require 'async'
require 'async/http/internet'
require 'protocol/http/body/file'

class Experiments::AsyncHTTP
  include Experiments
  
  def download
    profile("async-http", executions: EXECUTIONS, benchmark_path: BENCHMARK_DOWNLOAD_PATH) do
      Async do
        client = Async::HTTP::Internet.new
        
        response = client.get(DOWNLOAD_URL)
        response.each do |chunk|
          chunk.clear
        end
      ensure
        client&.close
      end
    end
  end

  def upload
    profile("async-http", executions: EXECUTIONS, benchmark_path: BENCHMARK_UPLOAD_PATH) do
      Async do
        client = Async::HTTP::Internet.new
        client.post(UPLOAD_URL, { accept: "text/plain" }, Protocol::HTTP::Body::File.open(tempfile_path))
      end
    end
  end
end