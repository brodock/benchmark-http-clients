class Experiments::Excon
  include Experiments

  def download
    profile("Excon", executions: EXECUTIONS, benchmark_path: BENCHMARK_DOWNLOAD_PATH) do
      streamer = lambda do |chunk, remaining_bytes, total_bytes|
        # this fails with EOFError
        #chunk.clear
      end
    
      ::Excon.get(DOWNLOAD_URL, :response_block => streamer)
    end
  end

  def upload
    profile("Excon", executions: EXECUTIONS, benchmark_path: BENCHMARK_UPLOAD_PATH) do
      tempfile.rewind

      chunker = lambda do
        # Excon.defaults[:chunk_size] defaults to 1048576, ie 1MB
        # to_s will convert the nil received after everything is read to the final empty chunk
        tempfile.read(::Excon.defaults[:chunk_size]).to_s
      end
    
      ::Excon.post(UPLOAD_URL, :request_block => chunker)
    end
  end
end