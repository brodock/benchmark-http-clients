class Experiments::HTTParty
  include Experiments

  def download
    profile("HTTParty", executions: EXECUTIONS, benchmark_path: BENCHMARK_DOWNLOAD_PATH) do
      ::HTTParty.get(DOWNLOAD_URL, stream_body: true) { |chunk| chunk.clear }
    end
  end

  def upload
    profile("HTTParty", executions: EXECUTIONS, benchmark_path: BENCHMARK_UPLOAD_PATH) do
      tempfile.rewind
      
      ::HTTParty.post(UPLOAD_URL, body: { file: tempfile })
    end
  end
end