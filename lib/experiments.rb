# frozen_string_literal: true

require 'rubygems'
require 'bundler/setup'
Bundler.require(:default)

require "tempfile"
require 'experiments'

module Experiments
  DOWNLOAD_URL = 'http://localhost:9292/download'
  UPLOAD_URL   = 'http://localhost:9292/upload'

  BENCHMARK_DOWNLOAD_PATH = 'download'
  BENCHMARK_UPLOAD_PATH   = 'upload'

  EXECUTIONS = 20

  # Returns a tempfile
  #
  # @return [Tempfile]
  def tempfile
    @tempfile ||= Tempfile.new.tap do |tmp|
      tmp.write 'a' * 10 * 1024 * 1024 # 10 MB
    end
  end

  def tempfile_path
    tempfile.path
  end

  def profile(name, executions: 1, benchmark_path:)
    fork do
      GC.disable
      memory_consumption(name) do
        executions.times do
          yield
        end
      end
    end
  
    Process.wait
  
    fork do
      memory_profiler(name, partial_path: benchmark_path) do
        executions.times do
          yield
        end
      end
    end
  
    Process.wait
  end
  
  def memory_consumption(name)
    memory_before = get_process_memory()
  
    result = yield
  
    memory_after = get_process_memory()
  
    total_memory = memory_after - memory_before
    puts "#{name}: #{"%.2f" % (total_memory.to_f / 1024)} MB"
  
    result
  end
  
  def memory_profiler(name, partial_path:)
    base_path = "benchmarks/#{partial_path}"
    FileUtils.mkdir_p(base_path) unless Dir.exist?(base_path)
    MemoryProfiler.start
  
    result = yield
  
    profiler = MemoryProfiler.stop
    profiler.pretty_print(scale_bytes: true, to_file: "#{base_path}/benchmark-#{name.downcase}.txt")
  
    result
  end
  
  def get_process_memory()
    `ps -p #{Process::pid} -o rss`.split("\n")[1].chomp.to_i
  end
end
