# Benchmark Ruby HTTP Clients

There is a [nice blog post](https://twin.github.io/httprb-is-great/) from Janko Marohnić that convinced me to try `http.rb`.

This benchmark code was based on [his own](https://gist.github.com/janko/238bbcc78b369ce3438365e5507bc671) and improved to
stress out the impact on the GC that memory hungry libraries will make.

When running it on my machine I got the following results:

## Ruby 2.6.x

with ruby 2.6: `ruby 2.6.6p146 (2020-03-31 revision 67876) [x86_64-darwin19]`

```ruby
rake download

Typhoeus: 8.69 MB
http.rb: 14.04 MB
async-http: 12.16 MB
Net::HTTP: 5.93 MB
Curb: 258.43 MB
RestClient: 12.79 MB
HTTParty: 273.09 MB
HTTPClient: 289.64 MB
Excon: 438.69 MB
```

```ruby
rake upload

Typhoeus: 5.66 MB
http.rb: 14.02 MB
async-http: 309.97 MB
Net::HTTP: 15.28 MB
Curb: 3.72 MB
RestClient: 222.54 MB
HTTParty: 807.04 MB
HTTPClient: 11.18 MB
Excon: 408.40 MB
```

Summary:

| Library    | Download (mem usage) | Upload (mem usage) |
|------------|----------------------|--------------------|
| Typhoeus   | 8.69 MB              | 5.66 MB            |
| http.rb    | 14.04 MB             | 14.02 MB           |
| async-http | 12.16 MB             | 309.97 MB          |
| Net::HTTP  | 5.93 MB              | 15.28 MB           |
| Curb       | 258.43 MB            | 3.72 MB            |
| RestClient | 12.79 MB             | 222.54 MB          |
| HTTParty   | 273.09 MB            | 807.04 MB          |
| HTTPClient | 289.64 MB            | 11.18 MB           |
| Excon      | 438.69 MB            | 408.40 MB          |


## Ruby 2.7.x

with ruby 2.7: `ruby 2.7.1p83 (2020-03-31 revision a0c7c23c9c) [x86_64-darwin19]`

```ruby
rake download

Typhoeus: 7.44 MB
http.rb: 12.51 MB
async-http: 8.57 MB
Net::HTTP: 5.05 MB
Curb: 258.00 MB
RestClient: 10.70 MB
HTTParty: 266.86 MB
HTTPClient: 289.28 MB
Excon: 437.75 MB
```

```ruby
rake upload

Typhoeus: 4.27 MB
http.rb: 10.21 MB
async-http: 303.95 MB
Net::HTTP: 14.33 MB
Curb: 14.44 MB
RestClient: 220.02 MB
HTTParty: 805.60 MB
HTTPClient: 8.02 MB
Excon: 407.38 MB
```

Summary:

| Library    | Download (mem usage) | Upload (mem usage) |
|------------|----------------------|--------------------|
| Typhoeus   | 7.44 MB              | 4.27 MB            |
| http.rb    | 12.51 MB             | 10.21 MB           |
| async-http | 8.57 MB              | 303.95 MB          |
| Net::HTTP  | 5.05 MB              | 14.33 MB           |
| Curb       | 258.00 MB            | 14.44 MB           |
| RestClient | 10.70 MB             | 220.02 MB          |
| HTTParty   | 266.86 MB            | 805.60 MB          |
| HTTPClient | 289.28 MB            | 8.02 MB            |
| Excon      | 437.75 MB            | 407.38 MB          |