# Run with `rackup app.ru`

require "roda"

CONTENT = ("a" * 10*1024*1024).freeze # 10 MB

Roda.route do |r|
  r.post "upload" do
    "Success"
  end

  r.get "download" do
    CONTENT
  end
end

run Roda.app
